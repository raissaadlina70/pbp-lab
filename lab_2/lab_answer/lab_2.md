1. Apakah perbedaan antara JSON dan XML?
                    JSON                                                |                       XML
- Merupakan object notation JavaScript                                  | -  Merupakan extensible markup langauage
- Tipe datanya bisa string, number, array, boolean                      | - Tipe datanya hanya string
- Tidak bisa menyisipkan comment                                        | - Dapat menyisipkan comment
- Kurang aman dibanding XML                                             | - Lebih aman
- Berorientasi pada data                                                | - Berorientasi pada dokumen
- Tidak memerlukan end tag                                              | - Menggunakan tag pada awal dan akhir
- Support array                                                         | - Tidak support array
- Hanya support text dan tipe data angka                                | - Supprot angka, text, gambar, tabel, grafik, dan lain-lain
                                        

2. Apakah perbedaan antara HTML dan XML?
                    HTML                                                |                      XML
- Hyper Text Markup Language                                            | - Extensible Markup Language
- HTML itu static (Tidak dapat diubah/diedit)                           | - XML itu dynamic (Dapat dirubah/diedit)
- Tidak memerlukan closing tag                                          | - Memerlukan closing tag
- Tidak case sensitive                                                  | - Merupakan case sensitive
- Jika terjadi sedikit error akan diabaikan                             | - Tidak bisa error sedikitpun
- Untuk menyajikan data                                                 | - Untuk transfer data














Referensi:
https://www.guru99.com/json-vs-xml-difference.html
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html
https://www.guru99.com/xml-vs-html-difference.html
https://www.geeksforgeeks.org/html-vs-xml/