from django.forms import fields
from .models import Note
from django import forms

import datetime
class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['to', 'fromm', 'title', 'message']

    to = forms.CharField(label="To", required=True, widget=forms.TextInput(attrs={"type" : "text"})) 
    fromm = forms.CharField(label="From", required=True, widget=forms.TextInput(attrs={"type" : "text"})) 
    title = forms.CharField(label="Title", required=True, widget=forms.TextInput(attrs={"type" : "text"}))
    message = forms.CharField(label="Message", required=True, widget=forms.TextInput(attrs={"type" : "text"}))