from django.shortcuts import render
from .forms import NoteForm
from .models import Note
from django.http.response import HttpResponseRedirect

# Create your views here.

def index(request):
    note = Note.objects.all().values()
    response = {'notes': note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/lab-4')
    context = {'form': form}
    return render(request, "lab4_form.html", context)

def note_list(request):
    response = {'notes' : Note.objects.all().values()}
    return render(request, "lab4_note_list.html",response)

