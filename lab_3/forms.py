# from django.db import models
from django.forms.models import InlineForeignKeyField
from .models import Friend
from django import forms
import datetime
class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']
    error_message = {
        'required' : 'Please Type'
    }
    input_attrs = {
        'type' : 'text',
        'placeholder' : 'Nama Kamu'
    }
    name = forms.CharField(label='Nama Lengkap Anda', required=True, max_length=50, widget=forms.TextInput(attrs={'type' : 'text', 'placeholder' : 'Nama Kamu'}))
    npm = forms.CharField(label='NPM', required=True, max_length=10, widget=forms.TextInput(attrs={'type' : 'text', 'placeholder' : 'NPM'}))
    dob = forms.DateField(label='DOB',initial=datetime.date.today)