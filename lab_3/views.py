from django.shortcuts import render
from .models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url = '/admin/login/')
def index(request):
    friends =  Friend.objects.all().values() # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url = '/admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None)
    if form.is_valid() and 'POST':
        form.save()
    context = {'form': form}
    return render(request, "lab3_form.html", context)