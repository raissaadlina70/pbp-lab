import 'package:flutter/material.dart';

import './dashboard_screen.dart';

class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  List<Map<String, Object>> _pages;
  int _selectedPageIndex = 0;

  @override
  void initState() {
    _pages = [
      {
        'page': DashboardScreen(),
        'title': 'Dashboard',
      },
    ];
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          TextButton(
          style: ButtonStyle(alignment: Alignment.centerLeft),
          child: Text('Dashboard',
          textAlign: TextAlign.start,
          ),
          ),
          TextButton(
            style: ButtonStyle(alignment: Alignment.centerLeft),
            child: Text('Forum',
            textAlign: TextAlign.start,
            ),
          ),
          TextButton(
            style: ButtonStyle(alignment: Alignment.centerLeft),
            child: Text('Berita',
            textAlign: TextAlign.start,
            ),
            onPressed: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => Screen2()));
          },
          ),
          TextButton(
            style: ButtonStyle(alignment: Alignment.centerLeft),
            child: Text('Rumah Sakit',
            textAlign: TextAlign.start,
            ),
          ),
          TextButton(
            style: ButtonStyle(alignment: Alignment.centerLeft),
            child: Text('Settings',
            textAlign: TextAlign.start,
            ),
          ),
        ],
      ),
      body: _pages[_selectedPageIndex]['page'],
    );
  }
}

class Screen2 extends StatefulWidget {
  @override
  _Screen2State createState() => _Screen2State();
}

class _Screen2State extends State<Screen2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.blueAccent),
      body: Center(
        child: TextButton(
          onPressed: () {
          },
          child: Text('Halaman Kosong'),
        ),
      ),
    );
  }
}